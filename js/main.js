// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 768 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});



$('.header-slider').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.review').slick({
    arrows: true,
    autoplay: false,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="review-nav prev"></span>',
    nextArrow: '<span class="review-nav next"></span>'
});

$(".btn-modal").fancybox({
    'padding'    : 0
});

